package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Application class for performing the game.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.06
 */
public class GameApp {
	
	/**
	 * Perform the program.
	 * @param args is not used
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
