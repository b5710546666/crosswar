package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Communicating with client.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.06
 */
public class Server extends AbstractServer {
	private List<ConnectionToClient> clients;
	Map<String,Integer> test;
	
	/**
	 * Constructor for new server.
	 * @param port is the number of port
	 */
	public Server(int port) {
		super(port);
		clients = new ArrayList<ConnectionToClient>();
	}	
	
	@Override
	protected void clientConnected(ConnectionToClient client) {
		clients.add(client);
		if(clients.size()<=2){
			if(clients.size()==2){
				sendToAllClients("gamestart");
			}
			try {
				client.sendToClient("Hi! player "+(clients.size()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			try {
				client.sendToClient("The room is already full, you are the observer");
				client.sendToClient(false);
			} catch (IOException e) {	
				e.printStackTrace();
			}		
		}
		super.clientConnected(client);
		if(clients.size()>=2){
			try {
				clients.get(0).sendToClient(true);
				clients.get(1).sendToClient(false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if(msg.getClass()!=String.class){
			if(client.equals(clients.get(0))){
				try {
					client.sendToClient(false);
					clients.get(1).sendToClient(true);
					clients.get(0).sendToClient("Player 2 turn...");
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}else{
				try {
					client.sendToClient(false);
					clients.get(0).sendToClient(true);
					clients.get(1).sendToClient("Player 1 turn...");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			sendToAllClients(msg);
		}else{
			String message = (String)msg;
			if(message.equals("pass")){
				if(client.equals(clients.get(0))){
					try {
						client.sendToClient(false);
						clients.get(1).sendToClient(true);
						clients.get(0).sendToClient("Player 2 turn...");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					try {
						client.sendToClient(false);
						clients.get(0).sendToClient(true);
						clients.get(1).sendToClient("Player 1 turn...");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				sendToAllClients(msg);
			}
		}
		
	}
}
