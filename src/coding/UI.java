package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * Reveal the result of the game in graphic user interface.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.06
 */
public class UI {
	private Map<String,List> map;
	JFrame frame;
	private String[] letterBag = {"a","b","c","d","e","f","g","h","i",
			"j","k","l","m","n","o","p","q","r",
			"s","t","u","v","w","x","y","z"};
	private String[] vowelBag = {"a","e","o","u","i"};
	private String[][]allKey;
	private String[] currentLetter;
	private boolean[][] checkPut;
	private JLabel[] currentletLabel;
	private JLabel[][] board;
	private JLabel[] currentBox;
	Player currentPlayer;
	private int indexChoosing=-1;
	private boolean[] currentPut;
	private int[][] indexPut;
	private String playerName,host;
	private static UI gui;
	JLabel hp,eHp;

	private String sentChat = "";
	private String chat = "";
	private JScrollPane scrollPane;	
	private JTextArea textArea;
	public boolean gameStarted,clicked;
	private JTextField textField;
	private JLabel ready,winORlose;
	protected MediaPlayer bgSound;
	protected Timer timer;

	/**
	 * Create the application.
	 * @param name is a user name
	 * @param host is a IP address
	 */
	public UI(String name,String host) {
		this.host = host;
		this.playerName = name;
		this.currentLetter = new String[8];
		this.currentletLabel = new JLabel[8];
		this.checkPut = new boolean[15][15];
		this.indexPut = new int[15][15];
		this.board = new JLabel[15][15];
		this.currentBox = new JLabel[8];
		this.gameStarted = false;
		this.map = new HashMap<String,List>();
		this.allKey = new String[15][15];
		this.clicked = false;
		this.bgSound = new MediaPlayer();
		this.gui = this;
		currentPut = new boolean[8];
		int i = 0;
		for(int a = 0;a<8;a++){
			i++;
			if(i==1){
				currentPut[a] = false;
				this.currentLetter[a] = this.letterBag[getRandomForVowel()];
				this.currentletLabel[a] = new JLabel();
				this.currentBox[a] = new JLabel();
			}else{
				currentPut[a] = false;
				this.currentLetter[a] = this.letterBag[getRandomInt()];
				this.currentletLabel[a] = new JLabel();
				this.currentBox[a] = new JLabel();
			}
		}
		for(int a = 0;a<15;a++){
			for(int b = 0;b<15;b++){
				allKey[a][b] = (a)+"_"+(b);
				this.checkPut[a][b] = false;
				indexPut[a][b] = -1;
				this.board[a][b] = new JLabel();
			}
		}
		initialize();
	}

	/**
	 * Random the new letters.
	 */
	public void randomNewWord(){
		int i = 0;
		for(int a = 0;a<currentPut.length;a++){
			i++;
			if(currentPut[a]&&i==1){
				currentLetter[a] = letterBag[getRandomForVowel()];
				currentletLabel[a].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[a]+".png")));
				currentPut[a] = false;
			}else if(currentPut[a]){
				currentLetter[a] = letterBag[getRandomInt()];
				currentletLabel[a].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[a]+".png")));
				currentPut[a] = false;
			}
		}
	}

	/**
	 * Random a number of vowel index.
	 * @return index of vowel array
	 */
	public int getRandomForVowel(){
		return (int)(Math.floor(Math.random()*(5)));
	}

	/**
	 * Random a number of twenty-six letters index.
	 * @return index of letters array
	 */
	public int getRandomInt() {
		return (int)(Math.floor(Math.random()*(26)));
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1075, 695);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		winORlose = new JLabel();
		winORlose.setBounds(0, 0, 1075, 695);
		frame.getContentPane().add(winORlose);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(655, 381, 395, 213);
		frame.getContentPane().add(scrollPane);

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);

		JButton btnNewButton = new JButton("Send");
		btnNewButton.setBounds(980, 594, 70, 32);
		btnNewButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {

				sentChat = playerName + " : " + textField.getText();
				textField.setText(null);
				try {
					currentPlayer.sendToServer(sentChat);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		});
		frame.getContentPane().add(btnNewButton);

		textField = new JTextField();
		textField.setBounds(655, 594, 325, 32);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel nameSign = new JLabel(this.playerName);
		nameSign.setBounds(850, 15, 130, 100);
		nameSign.setFont(new Font("Tahoma",Font.BOLD,20)); 
		nameSign.setForeground(Color.WHITE);
		frame.getContentPane().add(nameSign);

		JLabel player = new JLabel(new ImageIcon(UI.class.getResource("/pic/Player.png")));
		player.setBounds(695, 20 , 320 , 90);
		frame.getContentPane().add(player);

		hp = new JLabel(new ImageIcon(UI.class.getResource("/myOwnHP/hp10.png")));
		hp.setBounds(650, 100 , 250 ,150);
		frame.getContentPane().add(hp);

		eHp = new JLabel(new ImageIcon(UI.class.getResource("/enemyHP/ep10.png")));
		eHp.setBounds(760,280 , 200 , 100);
		frame.getContentPane().add(eHp);

		ready = new JLabel(new ImageIcon(UI.class.getResource("/button/Ready.png")));
		ready.setBounds(900, 100 , 150 , 70);
		frame.getContentPane().add(ready);
		ready.addMouseListener(new MouseListener(){
			public void mousePressed(MouseEvent e){
				clicked=true;
				currentPlayer = new Player(host,19541,gui,playerName);
				try {
					currentPlayer.openConnection();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if(!gameStarted){
					bgSound.buttonPress();
					ready.setIcon(new ImageIcon(UI.class.getResource("/button/Waiting3.png")));
					System.out.println("call");
					checkGame();
				}
			}
			public void mouseClicked(MouseEvent e){
				if(!gameStarted){
					ready.setIcon(new ImageIcon(UI.class.getResource("/button/Waiting3.png")));
				}
			}
			public void mouseEntered(MouseEvent e){
				if(!gameStarted&&!clicked){
					ready.setIcon(new ImageIcon(UI.class.getResource("/button/Ready2.png")));
				}
			}
			public void mouseExited(MouseEvent e){
				if(!gameStarted&&!clicked){
					ready.setIcon(new ImageIcon(UI.class.getResource("/button/Ready.png")));
				}
			}
			public void mouseReleased(MouseEvent e){
				if(!gameStarted&&!clicked){
					ready.setIcon(new ImageIcon(UI.class.getResource("/button/Ready2.png")));
				}
			}
		});

		JLabel pass = new JLabel(new ImageIcon(UI.class.getResource("/button/Pass.png")));
		pass.setBounds(900, 160 , 150 , 80);
		frame.getContentPane().add(pass);
		pass.addMouseListener(new MouseListener(){
			public void mousePressed(MouseEvent e){
				pass.setIcon(new ImageIcon(UI.class.getResource("/button/Pass3.png")));
				if(currentPlayer.started){
					bgSound.buttonPress();
					try {
						currentPlayer.sendToServer("pass");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}else{
					bgSound.buttonPress();
					gui.printMessage("not your turn, please wait...");
				}
			}
			public void mouseClicked(MouseEvent e){
				pass.setIcon(new ImageIcon(UI.class.getResource("/button/Pass2.png")));
			}
			public void mouseEntered(MouseEvent e){
				pass.setIcon(new ImageIcon(UI.class.getResource("/button/Pass2.png")));
			}
			public void mouseExited(MouseEvent e){
				pass.setIcon(new ImageIcon(UI.class.getResource("/button/Pass.png")));
			}
			public void mouseReleased(MouseEvent e){
				pass.setIcon(new ImageIcon(UI.class.getResource("/button/Pass2.png")));
			}
		});

		JLabel ok = new JLabel(new ImageIcon(UI.class.getResource("/pic/ok.png")));
		ok.setBounds(680, 300 , 100 , 80);
		frame.getContentPane().add(ok);
		ok.addMouseListener(new MouseListener(){
			public void mousePressed(MouseEvent e){
				bgSound.buttonPress();	
				ok.setIcon(new ImageIcon(UI.class.getResource("/pic/ok3.png")));
				try {
					ArrayList<List> pack = currentPlayer.packing(map, allKey);
					currentPlayer.sendToServer(pack);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				randomNewWord();
			}
			public void mouseClicked(MouseEvent e){
				ok.setIcon(new ImageIcon(UI.class.getResource("/pic/ok2.png")));
			}
			public void mouseEntered(MouseEvent e){
				ok.setIcon(new ImageIcon(UI.class.getResource("/pic/ok2.png")));
			}
			public void mouseExited(MouseEvent e){
				ok.setIcon(new ImageIcon(UI.class.getResource("/pic/ok.png")));
			}
			public void mouseReleased(MouseEvent e){
				ok.setIcon(new ImageIcon(UI.class.getResource("/pic/ok2.png")));
			}
		});


		for(int a = 0;a<8;a++){
			currentletLabel[a].setBounds(656+(50*a),245,43,43);
			frame.getContentPane().add(currentletLabel[a]);
			currentletLabel[a].addMouseListener(new CurrentBoxListener(a));
			currentBox[a].setIcon(new ImageIcon(UI.class.getResource("/pic/box.png")));
			currentBox[a].setBounds(650+(50*a),230,70,70);
			frame.getContentPane().add(currentBox[a]);
		}	
		for(int a = 0;a<15;a++){
			for(int b = 0;b<15;b++){
				board[a][b].setBounds(a*43,b*43,43,43);
				frame.getContentPane().add(board[a][b]);
				board[a][b].addMouseListener(new BoxListener(a,b));
			}
		}

		JLabel table = new JLabel(new ImageIcon(UI.class.getResource("/pic/table645.png")));
		table.setBounds(0, 0, 645, 645);
		frame.getContentPane().add(table);

		JLabel label_1 = new JLabel();
		label_1.setIcon(new ImageIcon(UI.class.getResource("/pic/BGsmoot.png")));
		label_1.setBounds(656, 11, 419, 593);
		frame.getContentPane().add(label_1);

		JLabel label = new JLabel();
		label.setIcon(new ImageIcon(UI.class.getResource("/pic/BGgame.png")));
		label.setBounds(0, 0, 1116, 654);
		frame.getContentPane().add(label);

	}

	/**
	 * Reveal player's turn
	 */
	public void setStartBut(){
		if(currentPlayer.started){
			ready.setIcon(new ImageIcon(UI.class.getResource("/button/yourturn.png")));
		}else{
			ready.setIcon(new ImageIcon(UI.class.getResource("/button/Waiting3.png")));
		}
	}

	/**
	 * Set the label icon of user depend on the HP value that received.
	 * @param hp is the value of HP
	 */
	public void setOwnHP(int hp){
		int a = howMuchHP(hp);
		this.hp.setIcon(new ImageIcon(UI.class.getResource("/myOwnHP/hp"+a+".png")));
	}

	/**
	 * Set the label icon of enemy depend on the HP value that received.
	 * @param hp is the value of HP
	 */
	public void setEnemyHP(int hp){
		int a = howMuchHP(hp);
		this.eHp.setIcon(new ImageIcon(UI.class.getResource("/enemyHP/ep"+a+".png")));
	}

	/**
	 * Check the image position by HP value.
	 * @param check is the HP value 
	 * @return the number of image position
	 */
	public int howMuchHP(int check){
		if(check>=90){
			return 10;
		}else if(check>=80){
			return 9;
		}else if(check>=70){
			return 8;
		}else if(check>=60){
			return 7;
		}else if(check>=50){
			return 6;
		}else if(check>=40){
			return 5;
		}else if(check>=30){
			return 4;
		}else if(check>=20){
			return 3;
		}else if(check>=10){
			return 2;
		}else if(check>0){
			return 1;
		}else{
			return 0;
		}
	}

	/**
	 * Get the list of words with position from the opponent and show it on this play own board.
	 * @param newLetter is an Arraylist of three index list
	 * 		  that early two index provide position in term of coordinate.
	 * 		  and the last is index of words( String ).
	 */
	public void putLetterFromEnemy(ArrayList<List> newLetter){
		for(int a = 0;a<newLetter.size();a++){
			checkPut[(int)newLetter.get(a).get(0)][(int)newLetter.get(a).get(1)] = true;
			board[(int)newLetter.get(a).get(0)][(int)newLetter.get(a).get(1)].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+newLetter.get(a).get(2)+".png")));
		}
	}

	/**
	 * Set the current labels according to current letters.
	 */
	public void setStart(){
		ready.setIcon(new ImageIcon(UI.class.getResource("/button/start3.png")));
		this.setCurrentLetter();
	}

	/**
	 * Reveal the image and perform the sound when the user win.
	 * @see image
	 */
	public void winner(){
		bgSound.winner();
		winORlose.setIcon(new ImageIcon(UI.class.getResource("/pic/youwin.png")));
	}

	/**
	 * Reveal the image and perform the sound when the user lose.
	 * @see image
	 */
	public void loser(){
		bgSound.loser();
		winORlose.setIcon(new ImageIcon(UI.class.getResource("/pic/youlose.png")));
	}

	/**
	 * Reveal the message on text area.
	 * @param msg is a message
	 */
	public void printMessage( String msg){
		this.chat+=msg+"\n";
		textArea.setText(chat);
	}

	/**
	 * Set current label of letters.
	 */
	public void setCurrentLetter(){
		for(int i = 0;i<8;i++){
			currentletLabel[i].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[i]+".png")));

		}
	}

	/**
	 * Check if the HP of player equals 0 will be the winner, otherwise will be the loser.
	 */
	public void update(){
		if(currentPlayer.ownHealth<=0){
			loser();
		}else if(currentPlayer.enemyHealth<=0){
			winner();
		}
	}

	/**
	 * Call update method each one second.
	 */
	public void checkGame(){
		this.timer = new Timer();
		this.timer.schedule(this.new Check(),0,500);
	}

	/**
	 * Update checkGame method.
	 * @author Apichaya Bunchongjit 5710546666
	 * 		   Natcha Pongsupanee 5710546224
	 * 		   Tuangrat Mungmeerattanaworachot 5710546241
	 */
	class Check extends TimerTask {
		public void run(){
			update();
		}
	}

	/**
	 * Action Listener for clicking the labels of letters on board.
	 * @author Apichaya Bunchongjit 5710546666
	 * 		   Natcha Pongsupanee 5710546224
	 * 		   Tuangrat Mungmeerattanaworachot 5710546241
	 *
	 */
	class BoxListener implements MouseListener {
		int a,b;
		public BoxListener(int a, int b){
			this.a = a;
			this.b = b;
		}

		/** Perform action when the mouse is pressed. */
		public void mousePressed(MouseEvent e){
			if(currentPlayer.started){
				bgSound.buttonPress();
				String key = a+"_"+b;
				List data = new ArrayList();
				if(indexChoosing!=-1){
					data.add(a);
					data.add(b);
					data.add(currentLetter[indexChoosing]);
				}
				if(!checkPut[a][b]){
					if(indexChoosing!=-1){
						indexPut[a][b] = indexChoosing;
						map.put(key, data);
						board[a][b].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[indexChoosing]+".png")));
						currentletLabel[indexChoosing].setIcon(null);
						checkPut[a][b] = true;
						currentPut[indexChoosing] = true; 
						indexChoosing=-1;
					}
				}
				else{
					map.remove(key);
					board[a][b].setIcon(null);
					currentletLabel[indexPut[a][b]].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[indexPut[a][b]]+".png")));
					checkPut[a][b] = false;
					currentPut[indexPut[a][b]] = false;
					indexPut[a][b] = -1;
				}
			}else{
				gui.printMessage("not your turn, please wait...");
			}
		}
		public void mouseClicked(MouseEvent e){}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
	};	

	/**
	 * Action Listener for clicking the current label of eight letters.
	 * @author Apichaya Bunchongjit 5710546666
	 * 		   Natcha Pongsupanee 5710546224
	 * 		   Tuangrat Mungmeerattanaworachot 5710546241
	 *
	 */
	class CurrentBoxListener implements MouseListener {
		int a;
		public CurrentBoxListener(int a ){
			this.a = a;
		}

		/** Perform action when the mouse is pressed. */
		public void mousePressed(MouseEvent e){
			indexChoosing = a;
			bgSound.buttonPress();
			for(int i = 0;i<8;i++){
				if(!currentPut[i]){
					bgSound.buttonPress();
					if(i==a){
						currentletLabel[i].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[i]+"2.png")));
					}else{
						currentletLabel[i].setIcon(new ImageIcon(UI.class.getResource("/letterPic/"+currentLetter[i]+".png")));
					}
				}
			}
		}
		public void mouseClicked(MouseEvent e){}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
	}
}