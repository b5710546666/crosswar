package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.io.BufferedInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * Play the audio file.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.06
 */
public class MediaPlayer {
	
	/**
	 * method to play music when itself be called
	 */
	public void playSound(){
		try{
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(
					 new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream("ringtone/loser.wav"))));
			clip.start();
			System.out.print("playing");
		}catch(Exception e){
			System.out.print("not playing");
		}
	}
	
	/**
	 * Play the audio when press the button.
	 */
	public void buttonPress(){
		try{
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(
					 new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream("ringtone/choose.wav"))));
			clip.start();
			System.out.print("playing");
		}catch(Exception e){
			System.out.print("not playing");
		}
	}
	
	/**
	 * Play the audio when the player lose the game.
	 */
	public void loser(){
		try{
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(
					 new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream("ringtone/loser.wav"))));
			clip.start();
			System.out.print("playing");
		}catch(Exception e){
			System.out.print("not playing");
		}
	}
	
	/**
	 * Play the audio when the player win the game.
	 */
	public void winner(){
		try{
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(
					 new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream("ringtone/winner.wav"))));
			clip.start();
			System.out.print("playing");
		}catch(Exception e){
			System.out.print("not playing");
		}
	}
}