package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * Reveal graphic user interface for input IP address of server.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.07
 */
public class IPinput {

	JFrame frame;
	private JTextField textField;
	private Login logui;
	private String name;
	
	/**
	 * Create the application.
	 */
	public IPinput(Login logui,String name) {
		this.logui = logui;
		this.name = name;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(41, 62, 190, 29);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnOk = new JButton("ok");
		btnOk.setBounds(92, 100, 89, 23);
		frame.getContentPane().add(btnOk);
		btnOk.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				logui.frame.setVisible(false);
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							UI window = new UI(name,textField.getText());
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}	
		});
		
		JLabel lblInputServersHost = new JLabel("input server's host");
		lblInputServersHost.setBounds(92, 11, 200, 50);
		frame.getContentPane().add(lblInputServersHost);
	}
}
