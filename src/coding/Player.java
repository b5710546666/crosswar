package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lloseng.ocsf.client.AbstractClient;

/**
 * The player class that extends AbstractClient.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.06
 */
public class Player extends AbstractClient {
	public boolean started;
	private String playerName;
	private static UI ui;
	private Map<String,Integer> consonants;
	private Map<String,Integer> wovel;
	public int ownHealth,enemyHealth;
	public Player(String host, int port,UI ui,String name){
		super(host, port);
		this.started = false;
		this.playerName = name;
		this.ui = ui;
		consonants = new HashMap<String,Integer>();
		consonants.put("b",4); consonants.put("c",4); consonants.put("d",2);
		consonants.put("f",4); consonants.put("g",3); consonants.put("h",3);
		consonants.put("j",10);consonants.put("k",5); consonants.put("l",2);
		consonants.put("m",4); consonants.put("n",2); consonants.put("p",4);
		consonants.put("q",50); consonants.put("r",1);consonants.put("s",1);
		consonants.put("t",1); consonants.put("v",5); consonants.put("w",4);
		consonants.put("x",8);consonants.put("z",30); consonants.put("y",4);
		wovel = new HashMap<String,Integer>();
		wovel.put("a",1); wovel.put("e",1);	wovel.put("i",1);wovel.put("o",1);
		wovel.put("u",2);
		ownHealth = 100;
		enemyHealth = 100;

	}
	
	@Override
	protected void handleMessageFromServer(Object msg) {
		if(msg.toString().equals("true")||msg.toString().equals("false")){
			boolean status = (boolean)msg;
			this.started = status;
			ui.setStartBut();
		}else if(msg.getClass()==String.class){
			String ms = (String) msg;
			if(ms.equals("gamestart")){
				ui.gameStarted = true;
				ui.setStart();
			}
				
			this.ui.printMessage(ms);
		}else{
			ArrayList<List> update = (ArrayList<List>)msg;
			isAttacked(update);
			
		}
		
		
		
	}
	
	/**
	 * Pack the data in map that sent from the UI to arrayList.
	 * @param map is data that dent from UI
	 * @param allKey is a key of map
	 * @return 
	 */
	public ArrayList<List> packing(Map<String, List> map,String[][] allKey){
		ArrayList<List> pack = new ArrayList<List>();
		for(int a = 0;a<allKey.length;a++){
			for(int b = 0;b<allKey.length;b++){
				if(map.containsKey(allKey[a][b])){
					pack.add(map.get(allKey[a][b]));
				}
			}
		}
		List name = new ArrayList();
		name.add(playerName);
		pack.add(name);

		return pack;
	}
		
	/**
	 * Count the score.
	 * @param letters is an array that has the array which store the locations and the letters.  
	 */
	public void isAttacked(ArrayList<List> letters){
		int vowelPoint = 0,consonantPoint = 0,healPoint = 0;
		boolean trip = false,doub = false;
		for(int a = 0;a<letters.size()-1;a++){
			switch(""+letters.get(a).get(0)+" "+letters.get(a).get(1)){
			//red tripple Score
			case "0 0": case "0 7": case "0 14": case "7 0": case "14 0":
			case "14 7": case "14 14": case "7 14": {
				if(checkVowel((String)letters.get(a).get(2))){
					vowelPoint += (int)(wovel.get((String)letters.get(a).get(2)));
					break;
				}else if(((String)letters.get(a).get(2)).equalsIgnoreCase("q")){
					healPoint+=40;
					break;
				}else if(((String)letters.get(a).get(2)).equalsIgnoreCase("z")){
					healPoint+= 20;
					break;
				}else{
					consonantPoint+=(int)(consonants.get((String)letters.get(a).get(2)));
					
					break;
				}
				
			}

			case "1 1": case "2 2": case "3 3": case "4 4": case "7 7" : case "10 10":
			case "11 11": case "12 12": case "13 13": case "1 13": case "2 12": case "3 11":
			case "4 10": case "10 4": case "11 3": case "12 2": case "13 1":{
				doub = true;
				if(checkVowel((String)letters.get(a).get(2))){
					vowelPoint += (int)(wovel.get((String)letters.get(a).get(2)));
					break;

				}else if(((String)letters.get(a).get(2)).equalsIgnoreCase("q")){
					healPoint+=40;
					break;

				}else if(((String)letters.get(a).get(2)).equalsIgnoreCase("z")){
					healPoint+= 20;
					break;
				}else{
					consonantPoint+=(int)(consonants.get((String)letters.get(a).get(2)));
					break;
				}
				
			}
			default :{
				if(checkVowel((String)letters.get(a).get(2))){
					vowelPoint += (int)(wovel.get((String)letters.get(a).get(2)));
					break;
				}else if(((String)letters.get(a).get(2)).equalsIgnoreCase("q")){
					healPoint+=40;
					break;
				}else if(((String)letters.get(a).get(2)).equalsIgnoreCase("z")){
					healPoint+= 20;
					break;
				}else{
					consonantPoint+=(int)(consonants.get((String)letters.get(a).get(2)));
					break;
				}
				
			}
			}
		}

		int totalScore = vowelPoint+consonantPoint;
		if(trip)totalScore*=3;
		if(doub)totalScore*=2;
		if(letters.get(letters.size()-1).get(0).equals(playerName)){
			this.enemyHealth-=totalScore;	
			this.ownHealth+=healPoint;
			ui.setEnemyHP(this.enemyHealth);
		}else{
			this.ownHealth -= totalScore;	
			ui.setOwnHP(this.ownHealth);
			ui.putLetterFromEnemy(letters);
		}

	}
	
	/**
	 * Check the vowel.
	 * @param letter is a letter that will be check
	 * @return true if the letter is a vowel, otherwise return false
	 */
	public boolean checkVowel(String letter){
		String[] vowel = {"a","e","i","o","u"};
		boolean check = false;
		for(int a = 0;a<5;a++){
			if(letter.equalsIgnoreCase(vowel[a])){
				check = true;
				break;
			}
		}
		return check;
	}
}