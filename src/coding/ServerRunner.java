package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

/**
 * Reveal the server of the game in graphic user interface.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.07
 */
public class ServerRunner {

	JFrame frame;
	private static final int PORT = 19541;
	
	/**
	 * Create the application.
	 */
	public ServerRunner() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("OPEN server");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(69, 50, 150, 52);
		frame.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Server server = new Server(PORT);
				try {
					server.listen();
					System.out.printf("Listening on port %d\n",
							PORT);
				} catch (IOException e) {
					System.out.println("Couldn't start server:");
					System.out.println(e);
				}
			}	
		});
	}

}
