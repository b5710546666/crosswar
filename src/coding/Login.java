package coding;

/*
 * This source code is Copyright 2015 by 
 * Apichaya Bunchongjit
 * Natcha Pongsupanee
 * Tuangrat Mungmeerattanaworachot.
 */
import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;

/**
 * Reveal the user interface of login.
 * @author Apichaya Bunchongjit 5710546666
 * 		   Natcha Pongsupanee 5710546224
 * 		   Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.05.06
 */
public class Login {

	JFrame frame;
	JLabel startBut,bg;
	private JTextField textField;
	public Login logui;

	/**
	 * Launch the application.
	 * @param args is not used
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		logui = this;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(Login.class.getResource("/pic/LoginBG.png")));
		frame.setBounds(100, 100, 300, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(122, 248, 139, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		startBut = new JLabel();
		startBut.setIcon(new ImageIcon(Login.class.getResource("/pic/startBut1.png")));
		startBut.setBounds(49, 292, 177, 41);
		frame.getContentPane().add(startBut);
		
		bg = new JLabel();
		bg.setIcon(new ImageIcon(Login.class.getResource("/pic/LoginBG.png")));
		bg.setBounds(0, 0, 284, 361);
		frame.getContentPane().add(bg);
		
		startBut.addMouseListener(new MouseListener(){
			public void mousePressed(MouseEvent e){
				startBut.setIcon(new ImageIcon(Login.class.getResource("/pic/startBut3.png")));
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							IPinput window = new IPinput(logui,textField.getText());
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
			public void mouseClicked(MouseEvent e){
				startBut.setIcon(new ImageIcon(Login.class.getResource("/pic/startBut2.png")));
			}
			public void mouseEntered(MouseEvent e){
				startBut.setIcon(new ImageIcon(Login.class.getResource("/pic/startBut2.png")));
			}
			public void mouseExited(MouseEvent e){
				startBut.setIcon(new ImageIcon(Login.class.getResource("/pic/startBut1.png")));
			}
			public void mouseReleased(MouseEvent e){
				startBut.setIcon(new ImageIcon(Login.class.getResource("/pic/startBut2.png")));
			}
		});
	}	
}
