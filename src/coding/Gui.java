package coding;

import java.awt.FlowLayout;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Gui extends JFrame {
	private JPanel pane1;
	private JLabel hourLabel1;
	private static int test = 0;
	public Gui(){
		initComponent();
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void update(){
		this.hourLabel1.setText(""+test);
	}
	public void initComponent(){
		pane1 = new JPanel();
		pane1.setLayout(new FlowLayout());
		hourLabel1 = new JLabel("mind");
		pane1.add(hourLabel1);
		super.add(pane1);
		this.pack();
		super.setVisible(true);
	}
	class Task extends TimerTask {
		public void run() {
			test++;
			update();
		}
	};
}
